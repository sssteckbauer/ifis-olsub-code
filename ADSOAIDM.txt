       IDENTIFICATION DIVISION.
FXXCHG PROGRAM-ID. ADSOAIDM.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       I-O-CONTROL.

       DATA DIVISION.
       FILE SECTION.

       WORKING-STORAGE SECTION.


      *----------------------------------------------------------------*
      *                                                                *
      *                   RECORD ADSOAIDM CODE-TABLE
      *                                                                *
      *----------------------------------------------------------------*
       01  ADSOAIDM-CODE-TABLE.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
      *----------------------------------------------------------------*
      *  Converted by Convert/ADSO Version 3.0  02/15/96               *
      *     Conversion Date/Time..................05/02/00 15:30:47.96
      *----------------------------------------------------------------*
           05  ADSOAIDM-WORK-FIELDS.
               10  ADSOAIDM-TABLE-TYPE       PIC X(7)
                           VALUE 'CODE'.
               10  ADSOAIDM-STATUS           PIC X(2)        VALUE
                SPACE.
                   88  ADSOAIDM-FOUND        VALUE 'F'.
                   88  ADSOAIDM-NOT-FOUND    VALUE 'NF'.
               10  ADSOAIDM-ENCODED-SEARCH-TYPE
                                            PIC X(1)        VALUE 'L'.
               10  ADSOAIDM-ENCODED-NF-VAL   PIC X(07)
                           VALUE 'UNKNOWN'.
               10  ADSOAIDM-ENCODED-NF-FLAG  PIC X(1)        VALUE 'Y'.
               10  ADSOAIDM-DECODED-SEARCH-TYPE
                                            PIC X(1)        VALUE 'L'.
               10  ADSOAIDM-DECODED-NF-VAL   PIC X(01)
                           VALUE ' '.
               10  ADSOAIDM-DECODED-NF-FLAG  PIC X(1)        VALUE 'N'.
           05  ADSOAIDM-TABLE-VALUES.


               10  FILLER                   PIC X(7)
                           VALUE 'PF1    '.
               10  FILLER                   PIC X(7)
                           VALUE '(PF1)  '.
               10  FILLER                   PIC X(7)
                           VALUE 'PF2    '.
               10  FILLER                   PIC X(7)
                           VALUE '(PF2)  '.
               10  FILLER                   PIC X(7)
                           VALUE 'PF3    '.
               10  FILLER                   PIC X(7)
                           VALUE '(PF3)  '.
               10  FILLER                   PIC X(7)
                           VALUE 'PF4    '.
               10  FILLER                   PIC X(7)
                           VALUE '(PF4)  '.
               10  FILLER                   PIC X(7)
                           VALUE 'PF5    '.
               10  FILLER                   PIC X(7)
                           VALUE '(PF5)  '.
               10  FILLER                   PIC X(7)
                           VALUE 'PF6    '.
               10  FILLER                   PIC X(7)
                           VALUE '(PF6)  '.
               10  FILLER                   PIC X(7)
                           VALUE 'PF7    '.
               10  FILLER                   PIC X(7)
                           VALUE '(PF7)  '.
               10  FILLER                   PIC X(7)
                           VALUE 'PF8    '.
               10  FILLER                   PIC X(7)
                           VALUE '(PF8)  '.
               10  FILLER                   PIC X(7)
                           VALUE 'PF9    '.
               10  FILLER                   PIC X(7)
                           VALUE '(PF9)  '.
               10  FILLER                   PIC X(7)
                           VALUE 'PF10   '.
               10  FILLER                   PIC X(7)
                           VALUE '(PF10) '.
               10  FILLER                   PIC X(7)
                           VALUE 'PF11   '.
               10  FILLER                   PIC X(7)
                           VALUE '(PF11) '.
               10  FILLER                   PIC X(7)
                           VALUE 'PF12   '.
               10  FILLER                   PIC X(7)
                           VALUE '(PF12) '.
               10  FILLER                   PIC X(7)
                           VALUE 'PF13   '.
               10  FILLER                   PIC X(7)
                           VALUE '(PF13) '.
               10  FILLER                   PIC X(7)
                           VALUE 'PF14   '.
               10  FILLER                   PIC X(7)
                           VALUE '(PF14) '.
               10  FILLER                   PIC X(7)
                           VALUE 'PF15   '.
               10  FILLER                   PIC X(7)
                           VALUE '(PF15) '.
               10  FILLER                   PIC X(7)
                           VALUE 'PF16   '.
               10  FILLER                   PIC X(7)
                           VALUE '(PF16) '.
               10  FILLER                   PIC X(7)
                           VALUE 'PF17   '.
               10  FILLER                   PIC X(7)
                           VALUE '(PF17) '.
               10  FILLER                   PIC X(7)
                           VALUE 'PF18   '.
               10  FILLER                   PIC X(7)
                           VALUE '(PF18) '.
               10  FILLER                   PIC X(7)
                           VALUE 'PF19   '.
               10  FILLER                   PIC X(7)
                           VALUE '(PF19) '.
               10  FILLER                   PIC X(7)
                           VALUE 'PF20   '.
               10  FILLER                   PIC X(7)
                           VALUE '(PF20) '.
               10  FILLER                   PIC X(7)
                           VALUE 'PF21   '.
               10  FILLER                   PIC X(7)
                           VALUE '(PF21) '.
               10  FILLER                   PIC X(7)
                           VALUE 'PF22   '.
               10  FILLER                   PIC X(7)
                           VALUE '(PF22) '.
               10  FILLER                   PIC X(7)
                           VALUE 'PF23   '.
               10  FILLER                   PIC X(7)
                           VALUE '(PF23) '.
               10  FILLER                   PIC X(7)
                           VALUE 'PF24   '.
               10  FILLER                   PIC X(7)
                           VALUE '(PF24) '.
               10  FILLER                   PIC X(7)
                           VALUE 'PA1    '.
               10  FILLER                   PIC X(7)
                           VALUE '(PA1)  '.
               10  FILLER                   PIC X(7)
                           VALUE 'CLEAR  '.
               10  FILLER                   PIC X(7)
                           VALUE '(CLEAR)'.
           05  ADSOAIDM-TABLE-ENTRIES-ALL
                            REDEFINES ADSOAIDM-TABLE-VALUES.
               10  ADSOAIDM-TABLE-ENTRIES
                    OCCURS 26 INDEXED BY
                             ADSOAIDM-INDEX.
                   15  ADSOAIDM-ENCODED-VALUE
                                        PIC X(7).
                   15  ADSOAIDM-DECODED-VALUE
                                        PIC X(7).
       LINKAGE SECTION.
       01  LINK-METHOD                  PIC X(8).
       01  STATUS-FLAG                  PIC X(1).
       01  WK-FIELD1                    PIC X(80).
       01  WK-FIELD2                    PIC X(80).

       PROCEDURE DIVISION USING LINK-METHOD
                                STATUS-FLAG
                                WK-FIELD1
                                WK-FIELD2.

       000-MAIN.

           EVALUATE LINK-METHOD
              WHEN 'ENCODE'
                 PERFORM 100-ENCODE
              WHEN 'DECODE'
                 PERFORM 100-DECODE
              WHEN OTHER
                 MOVE '?' TO STATUS-FLAG
           END-EVALUATE.

           GOBACK.

       100-ENCODE.
      ******************************************************************
      *************  ENCODE
      ******************************************************************
           SET ADSOAIDM-INDEX TO +1.
            SEARCH ADSOAIDM-TABLE-ENTRIES
                AT END
                    MOVE 'NF' TO ADSOAIDM-STATUS
                    MOVE ADSOAIDM-DECODED-NF-VAL TO
                      WK-FIELD1
                    MOVE 'N' TO STATUS-FLAG
                WHEN ADSOAIDM-DECODED-VALUE(ADSOAIDM-INDEX) =
                      WK-FIELD2
                    MOVE 'F' TO ADSOAIDM-STATUS
                    MOVE ADSOAIDM-ENCODED-VALUE(ADSOAIDM-INDEX) TO
                      WK-FIELD1
                    MOVE 'Y' TO STATUS-FLAG
            END-SEARCH.

       100-DECODE.
      ******************************************************************
      *************  DECODE
      ******************************************************************
           SET ADSOAIDM-INDEX TO +1.
           SEARCH ADSOAIDM-TABLE-ENTRIES
               AT END
                   MOVE 'NF' TO ADSOAIDM-STATUS
                   MOVE ADSOAIDM-ENCODED-NF-VAL TO
                     WK-FIELD2
                   MOVE 'N' TO STATUS-FLAG
               WHEN ADSOAIDM-ENCODED-VALUE(ADSOAIDM-INDEX) =
                    WK-FIELD1
                   MOVE 'F' TO ADSOAIDM-STATUS
                   MOVE ADSOAIDM-DECODED-VALUE(ADSOAIDM-INDEX) TO
                     WK-FIELD2
                   MOVE 'Y' TO STATUS-FLAG
           END-SEARCH.



